#include <stdio.h>
#include "getch.h"
void win(int who) {
  printf("\033[16;3H");
  switch (who) {
    case 0:
      printf("black wins\n");
      break;
    case 1:
      printf("white wins\n");
      break;
  }
  printf("\033[17;3H<press any key>");
  getch();
}
