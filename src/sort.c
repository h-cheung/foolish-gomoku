#include <stdlib.h>
#include <string.h>
#include "pair.h"
#include "pow.h"
#include "update.h"
const extern int b[17][17];
extern struct pair points[225];
extern const __int128 INF;
__int128 value[16][16];
int cmp(const void *a, const void *b) {
  return value[((const struct pair *)a)->y][((const struct pair *)a)->x] <
         value[((const struct pair *)b)->y][((const struct pair *)b)->x];
}
int sortpoints(const int l[2][16][16], const int who) {
  for (int i = 1; i <= 15; ++i)
    for (int j = 1; j <= 15; ++j)
      if (b[i][j])
        value[i][j] = -INF;
      else if (l[who][i][j] >= 11 || l[!who][i][j] >= 11)
        value[i][j] =
            (INF << 4) + (l[who][i][j] + 2 > l[!who][i][j] ? l[who][i][j] + 2
                                                           : l[!who][i][j]);
      else
        value[i][j] = qpow(10, l[who][i][j]) + qpow(10, l[!who][i][j] + 2) +
                      (i < 16 - i ? i : 16 - i) + (j < 16 - j ? j : 16 - j);
  // for (int i = 0; i < 225; ++i) printf("%d %d ",points[i].y,
  // points[i].x);
  qsort(points, 225, sizeof(struct pair), cmp);
  return value[points->y][points->x] >= INF << 4;
}
