__int128 ans, tmp;
__int128 qpow(const int x, const __int128 n) {
  if (!n)
    ans = 1;
  else if (n == 1)
    ans = x;
  else {
    tmp = qpow(x, n / 2);
    ans = tmp * tmp;
    if (n % 2) ans = ans * x;
  }
  return ans;
}
