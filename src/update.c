#include <stdio.h>
#include <string.h>
#include "pair.h"
#include "pow.h"
#include "rangejudge.h"
#define EY epos[i].y
#define EX epos[i].x
int visited[17][17];
const extern int b[17][17];
extern const __int128 INF;
int l[4];
__int128 ans;
int forward[8][2] = {{-1, -1}, {-1, 0}, {-1, 1}, {0, 1},
                     {1, 1},   {1, 0},  {1, -1}, {0, -1}};
__int128 _update(int who, const struct pair pos, int len[2][16][16], int dep) {
  visited[pos.y][pos.x] = 1;
  *l = l[1] = l[2] = l[3] = 3;
  struct pair epos[8];
  for (int i = 0; i < 8; ++i) {
    for (EY = pos.y + forward[i][0], EX = pos.x + forward[i][1];
         rangejudge(EX, EY) && b[EY][EX] == who + 1;
         EY = EY + forward[i][0], EX = EX + forward[i][1])
      l[i % 4] += 3;
  }
  ans = 0;
  for (int i = 0; i < 4; ++i)
    if (l[i] >= 15) return INF;
  for (int i = 0; i < 8; ++i)
    if (!b[EY][EX]) ++l[i % 4];
  for (int i = 0; i < 8; ++i)
    if (!b[EY][EX]) {
      // if (rangejudge(EY, EY) && !dep && !visited[EY][EX] &&
      //    b[EY + forward[i][0]][EX + forward[i][1]])
      //  ans += _update(who, make_pair(EY, EX), len, 0);
      if (len[who][EY][EX] < l[i % 4]) {
        ans += qpow(10, l[i % 4]) - qpow(10, len[who][EY][EX]);
        len[who][EY][EX] = l[i % 4];
      }
    }
  return ans;
}

__int128 update(int who, const struct pair pos, int len[2][16][16]) {
  memset(visited, 0, sizeof(visited));
  __int128 ans = 0;
  if ((ans += _update(who, pos, len, 0)) != INF)
    for (int i = 0; i < 8; ++i) {
      struct pair epos;
      for (epos.y = pos.y + forward[i][0], epos.x = pos.x + forward[i][1];
           rangejudge(epos.x, epos.y) && b[epos.y][epos.x] == 1 + !who;
           epos.y = epos.y + forward[i][0], epos.x = epos.x + forward[i][1])
        ;
      if (!b[epos.y][epos.x]) ans += qpow(10, len[!who][epos.y][epos.x]--);
    }
  return ans;
}
