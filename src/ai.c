#include <string.h>
#include "pow.h"
#include "sort.h"
#include "update.h"
#define WIDTH 12
#define MDEPTH 4
extern int b[17][17];
extern const __int128 INF;
extern const int len[2][16][16];
extern const struct pair points[225];
int tmp[MDEPTH + 1][2][16][16];
__int128 ans[MDEPTH + 1], M[MDEPTH + 1], MM;
__int128 dfs(const int l[2][16][16], const struct pair now, int who,
             int depth) {
  ans[depth] = update(!who, now, tmp[depth]) + update(who, now, tmp[depth]);
  b[now.y][now.x] = who + 1;
  for (int i = 0; i < 2; ++i)
    for (int j = 1; j < 16; ++j)
      for (int k = 1; k < 16; ++k) tmp[depth][i][j][k] = len[i][j][k];
  tmp[depth][0][now.y][now.x] = tmp[depth][1][now.y][now.x] = -1;
  if (ans[depth] >= INF) {
    b[now.y][now.x] = 0;
    return ans[depth] << 4;
  } else if (depth == MDEPTH) {
    b[now.y][now.x] = 0;
    return ans[depth];
  }
  if (sortpoints(tmp[depth], who)) return INF << 1;
  struct pair pre[WIDTH];
  for (int i = 0; i < WIDTH; ++i) pre[i] = points[i];
  M[depth] = -INF;
  for (int i = 0; i < WIDTH; ++i) {
    int t = dfs(tmp[depth], pre[i], !who, depth + 1);
    M[depth] = M[depth] > t ? M[depth] : t;
  }
  b[now.y][now.x] = 0;
  return ans[depth] - M[depth] / 20;
}
struct pair ai(int player) {
  if (sortpoints(len, player)) return *points;
  struct pair pre[WIDTH], tmp;
  for (int i = 0; i < WIDTH; ++i) pre[i] = points[i];
  MM = -INF;
  for (int i = 0; i < WIDTH; ++i) {
    int t = dfs(len, pre[i], player, 0);
    if (t > MM) {
      MM = t;
      tmp = pre[i];
    }
  }
  return tmp;
}
