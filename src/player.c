#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "ai.h"
#include "getch.h"
#include "pair.h"
#include "pow.h"
#include "sort.h"
#include "update.h"
extern int b[17][17];
extern const int len[2][16][16];
extern const struct pair points[225];
struct pair play(int player, struct pair now) {
  if (player >= 2) return ai(player - 2);
  while (1) {
    printf("\033[%d;%dH", now.y, now.x);
    int tmp = getch();
    switch (tmp) {
      case 68:
        if (now.x > 1) --now.x;
        break;
      case 67:
        if (now.x < 15) ++now.x;
        break;
      case 65:
        if (now.y > 1) --now.y;
        break;
      case 66:
        if (now.y < 15) ++now.y;
    }
    if (tmp == 32 || tmp == 13) return now;
  }
}
