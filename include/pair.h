#ifndef FOOLISH_GOMOKU_INCLUDE_PAIR_H
#define FOOLISH_GOMOKU_INCLUDE_PAIR_H
struct pair {
  int y, x;
};
struct pair make_pair(int y, int x);
#endif                               // FOOLISH_GOMOKU_INCLUDE_PAIR_H
