#ifndef FOOLISH_GOMOKU_INCLUDE_POW_H
#define FOOLISH_GOMOKU_INCLUDE_POW_H
__int128 qpow(const int x, const __int128 n);
#endif
