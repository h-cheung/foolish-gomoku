//
// Created by hc on 19-5-29.
//
#ifndef FOOLISH_GOMOKU_INCLUDE_PLAYER_H
#define FOOLISH_GOMOKU_INCLUDE_PLAYER_H
#include "pair.h"
struct pair play(int now, struct pair pos);
#endif  // FOOLISH_GOMOKU_INCLUDE_PLAYER_H
