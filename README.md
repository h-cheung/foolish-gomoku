# foolish-backgammon

a simple and foolish backgammon AI in C, which only depends on unix shell and is tested on linux complied by llvm clang 8.0.0 and by gcc 8.3.0

[![996.icu](https://img.shields.io/badge/link-996.icu-red.svg?style=flat-square)](https://996.icu)
[![LICENSE](https://img.shields.io/badge/license-Anti%20996-blue.svg?style=flat-square)](https://github.com/996icu/996.ICU/blob/master/LICENSE)
[![MIT Licence](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](https://opensource.org/licenses/mit-license.php)

## FEATURE

- [x] main menu
- [x] game board UI in terminal
- [x] basic operation ~~and simple judgement~~
- [x] quick judgement

  *about 2.5 hours for all above*
- [x] simple and foolish AI
- [x] single player, two players and just AI vs AI

  *about 11 hours*

## TODO(maybe, volue 0 means must be done)

| item                                | value                         | planned time |
| ----------------------------------- | ----------------------------- | ------------ |
| save and load                       | 7                             | 1 hour   |
| repent                              | 6                             | 3 hours      |
| balance breaker                     | 5                             | 5 hours      |
| vim-like keybindings                | 2                             | 1 hour      |
|                                     | *(the game without AI ended)* |              |
| make the AI more efficive and smart | 8                             | 8 hours      |
| AI for balanced breaker             | 5                             | 6 hours      |
| ……                                  |

## Build

You can use cmake to build this project, please make sure your compiler support __int128. On linux, both clang 8.0.0 x64 with libstdc++ (recommanded) and GCC 8.3.0 x64 are tested. the simplist way is that run `cmake .` and then run `make` then the executable file will be at `./foolish-gomoku`

## Run

After build, you can directly run the executable file in a unix shell(zsh, bash, etc). To get best experince, please use a wide font whose width is equal to its height. `Misc Fixed Wide`, which should be installed as depandancy when installing xorg, is OK. You can press arrow key to move and than press blankspace or enter.

## Known Issues

- in a few rare cases, AI may cause segmentation fault or infinite loop (the latter seems to have been fixed)
- some times the AI can be very foolish
