//
// Created by hc on 19-5-29.
//

#ifndef FOOLISH_GOMOKU_INCLUDE_UPDATE_H
#define FOOLISH_GOMOKU_INCLUDE_UPDATE_H
#include "pair.h"
__int128 update(int who, struct pair pos, int len[2][16][16]);
#endif  // FOOLISH_GOMOKU_INCLUDE_UPDATE_H
