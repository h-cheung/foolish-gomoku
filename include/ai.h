#ifndef FOOLISH_GOMOKU_INCLUDE_AI_H
#define FOOLISH_GOMOKU_INCLUDE_AI_H
#include "pair.h"
__int128 dfs(const int l[2][16][16], const struct pair now, int who,
                    int depth);
struct pair ai(int player);
#endif
