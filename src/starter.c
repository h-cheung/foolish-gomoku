#include <stdio.h>
#include <string.h>
#include "board.h"
#include "pair.h"
#include "player.h"
#include "update.h"
#include "win.h"
int b[17][17];
extern const __int128 INF;
int len[2][16][16];
void game(int AI_enabled) {
  memset(b, 0, sizeof(b));
  memset(len, 0, sizeof(len));
  for (int i = 2; i < 15; ++i)
    for (int j = 2; j < 15; ++j) len[0][i][j] = len[1][i][i] = 1;
  int a[2] = {0, 1};
  switch (AI_enabled) {
    char tmp[2];
    case 1:
      printf("use black or white?\n      1)black\ndefault)white");
      fgets(tmp, 2, stdin);
      a[!((int)*tmp - '1')] += 2;
      break;
    case 0:
      a[0] += 2;
      a[1] += 2;
  }
  int who = 0;
  struct pair now = {1, 1};
  show_board();
  while (1) {
    printf("\033[16;3H");
    if (who)
      puts("now white");
    else
      puts("now black");
    do
      now = play(a[who], now);
    while (b[now.y][now.x]);
    printf("\033[%d;%dH", now.y, now.x);
    b[now.y][now.x] = 1 + who;
    len[0][now.y][now.x] = len[1][now.y][now.x] = -1;
    show_board();
    if (update(who, now, len) == INF) {
      win(who);
      return;
    }
    who = !who;
  }
}
