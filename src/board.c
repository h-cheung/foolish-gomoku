#include "board.h"
#include <stdio.h>
#include <wchar.h>
const extern int b[17][17];
void show_point(int i, int j) {
  if (i == 1) {
    if (j==1)
      printf("┌");
    else if (j == 15)
      printf("┐");
    else
      printf("┬");
  } else if (i == 15) {
    if (j==1)
      printf("└");
    else if (j == 15)
      printf("┘");
    else
      printf("┴");
  } else {
    if (j==1)
      printf("├");
    else if (j == 15)
      printf("┤");
    else
      printf("┼");
  }
}
void show_board() {
  printf("\033[2J\033[H");
  for (int i = 1; i < 16; ++i) {
    printf("\033[%d;%dH", i , 0);
    for (int j = 1; j < 16; ++j) switch (b[i][j]) {
        case 0:
          show_point(i, j);
          break;
        case 1:
          printf("●");
          break;
        case 2:
          printf("○");
      }
    putchar('\n');
  }
}
