#include <stdio.h>
#include "getch.h"
#include "init.h"
#include "starter.h"
int main() {
  init();
  while (1) {
    printf(
        "\033[2J"
        "\033[H"
        "WELCOME TO FOOLISH BACKGAMMON!\n"
        "1)  Single Player\n"
        "2)  Multi Player\n"
        "3)  AI vs AI\n"
        "0)  Exit\n");
    char a[3];
    fgets(a, 3, stdin);
    if (a[0] == '1' && a[1] == '\n') {
      game(1);
      continue;
    }
    else if (a[0] == '2' && a[1] == '\n') {
      game(2);
      continue;
    }
    else if (a[0] == '3' && a[1] == '\n') {
      game(0);
      continue;
    }
    else if (a[0] == '0' && a[1] == '\n') {
      return 0;
    } else
      printf("Invailed input, please try again! <PRESS ANY KEY>\n");
    getch();
  }
}
